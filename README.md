# IP Masters/Doctoral Thesis 
- Latex Template for Master or phD Thesis
- Default format B5 , can be changed in line 217 in main.tex -> change the font size then also
- Example files for code etc. provided

## Getting started
- Change Cover.pptx and export as cover.pdf
- Change Title and Name in Main
- Change / Add Chapters
- Remove Statutory Declaration in Lieu of an Oath if not needed 

```
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% IP Masters/Doctoral Thesis 
% LaTeX Template
% Version 0.1 (06/06/24)
%
% This template is based on a template by:
% Steve Gunn (http://users.ecs.soton.ac.uk/srg/softwaretools/document/templates/)
% Sunil Patel (http://www.sunilpatel.co.uk/thesis-template/)
%
% Template license:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
```