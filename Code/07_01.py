import stardog
#setup connection details
conn_details = {
  'endpoint': 'http://endpointadresse.example/store',
  'username': 'admin',
  'password': '*********'
}
#insert SPARQL query string
querystring = ' select * { ?a ?p ?o. } '
#send query to triple store
with stardog.Connection('DatabaseName', **conn_details) as conn:
    conn.begin()
    data = conn.select(querystring, content_type='application/sparql-results+json')